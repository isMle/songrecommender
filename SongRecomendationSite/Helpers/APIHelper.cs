﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Xml.Linq;
using SongRecommendationSite.Models;

namespace SongRecommendationSite.Helpers
{
    public class APIHelper
    {
        private const string TERM_API_BASE_URL = "http://developer.echonest.com/api/v4/artist/";
        private const string SEARCH_API_BASE_URL = "http://developer.echonest.com/api/v4/song/";
        private const string API_TOKEN = "6DVLSUX99YDAESKAX";
        private const string TERMS_ENDPOINT = "list_terms";
        private const string SEARCH_ENDPOINT = "search";

        public static List<string> GetTermsFromAPI(string type)
        {
            var xml = CallAPI(TERM_API_BASE_URL, TERMS_ENDPOINT, $"&type={type}");
            return GetListOfValues(xml);
        }

        private static List<string> GetListOfValues(XContainer xml)
        {
            if (xml == null || xml.Descendants().All(e => e.Name != "name"))
                return new List<string>();
            var listOfResults = xml.Descendants("name").Select(n => n.Value).ToList();
            return listOfResults;
        }

        public static XElement GetRecommendationFromAPI(FormData data)
        {
            var urlParams = "&results=1";
            urlParams = AddUrlParameter("artist", data.Artist, urlParams);
            urlParams = AddUrlParameter("title", data.Title, urlParams);
            urlParams = AddUrlParameter("style", data.Style, urlParams);
            urlParams = AddUrlParameter("mood", data.Mood, urlParams);
            var xml = CallAPI(SEARCH_API_BASE_URL, SEARCH_ENDPOINT, urlParams);
            return xml;
        }

        private static string AddUrlParameter(string  param, string value, string urlParams)
        {
            if (string.IsNullOrWhiteSpace(value))
                return urlParams;
            urlParams += $"&{param}={value.Replace(" ", "%20")}";
            return urlParams;
        }

        private static XElement CallAPI(string baseUrl, string endpoint, string urlParams)
        {
            var client = new HttpClient { BaseAddress = new Uri(baseUrl) };
            var urlExtension = $"{endpoint}?api_key={API_TOKEN}&format=xml{urlParams}";
            HttpResponseMessage response;
            try
            {
                response = client.GetAsync(urlExtension).Result;
            }
            catch (Exception ex)
            {
                throw new Exception($"Unknown exception caught when calling API.\r\n{ex.Message}\r\n{ex.StackTrace}");

            }
            if (response == null || !response.IsSuccessStatusCode) return null;
            var xml = response.Content.ReadAsStringAsync();
            return XElement.Parse(xml.Result);
        }
    }
}