﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SongRecommendationSite.Helpers;
using SongRecommendationSite.Models.ViewModels;

namespace SongRecommendationSite.Controllers
{
    public class HomeController : Controller
    {
        private const string MOOD = "mood";
        private const string STYLE = "style";

        public ActionResult Index()
        {
            var moods = APIHelper.GetTermsFromAPI(MOOD);
            var styles = APIHelper.GetTermsFromAPI(STYLE);
            var model = new HomeViewModel
            {
                Moods = GetListOfItems(moods),
                Styles = GetListOfItems(styles)
            };
            return View(model);
        }

        private static SelectList GetListOfItems(IReadOnlyCollection<string> list)
        {
            var result = new List<SelectListItem> { new SelectListItem
                        {
                            Value = string.Empty,
                            Text = string.Empty
                        }};
            if (list.Any())
                result.AddRange(GetItemsFromList(list));
            return new SelectList(result, "Value", "Text");
        }

        private static IEnumerable<SelectListItem> GetItemsFromList(IEnumerable<string> list)
        {
            return list
                .Select(x =>
                    new SelectListItem
                    {
                        Value = x,
                        Text = x
                    });
        }
    }
}