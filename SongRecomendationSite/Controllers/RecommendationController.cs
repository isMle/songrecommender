﻿using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;
using SongRecommendationSite.Helpers;
using SongRecommendationSite.Models;
using SongRecommendationSite.Models.ViewModels;

namespace SongRecommendationSite.Controllers
{
    public class RecommendationController : Controller
    {
        [HttpPost]
        public ActionResult GetRecommendation(FormData data)
        {
            var recommendation = APIHelper.GetRecommendationFromAPI(data);
            if (RecommendationNotValid(recommendation))
                return View(new RecommendationViewModel()); ;
            var model = new RecommendationViewModel
            {
                Artist = recommendation.Descendants("artist_name").FirstOrDefault().Value,
                Title = recommendation.Descendants("title").FirstOrDefault().Value
            };
            return View(model);
        }

        private static bool RecommendationNotValid(XContainer recommendation)
        {
            return recommendation == null || recommendation.Descendants().All(e => e.Name != "artist_name")
                   || recommendation.Descendants().All(e => e.Name != "title");
        }
    }
}