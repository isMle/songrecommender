﻿namespace SongRecommendationSite.Models.ViewModels
{
    public class RecommendationViewModel
    {
        public string Artist { get; set; }
        public string Title { get; set; }
    }
}