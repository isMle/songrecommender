﻿using System.Web.Mvc;

namespace SongRecommendationSite.Models.ViewModels
{
    public class HomeViewModel
    {
        public SelectList Styles { get; set; }
        public string CurrentStyle { get; set; }
        public SelectList Moods { get; set; }
        public string CurrentMood { get; set; }
        public FormData Data { get; set; }
    }
}