﻿namespace SongRecommendationSite.Models
{
    public class FormData
    {
        public string Artist { get; set; }
        public string Title { get; set; }
        public string Mood { get; set; }
        public string Style { get; set; }
    }
}